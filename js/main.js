$(function () {
    /* preloader */
    $(window).load(function () {
        $('.preloader').addClass('preloader-hidden');
        setTimeout(function () {
            $('.preloader').remove();
        }, 1000);
    });


    // Elements to inject
    var mySVGsToInject = $('img[src$="svg"]');
    // Do the injection
    SVGInjector(mySVGsToInject);

    $('.svg-map g')
            .on('mouseenter',
                    function () {
                        $('.svg-map g').attr('data-hover', 'false');
                        $(this).attr('data-hover', 'true');
                    }
            )
            .on('mouseleave',
                    function () {
                        $('.svg-map g').removeAttr('data-hover');
                    }
            );


    $('footer .arrow-top').click(function () {
        $.scrollify.move('#main');
    });

    $('.button').each(function () {
        var html = $(this).html();
        $(this).html('<span>' + html + '</span>');
    });


    $('a[href=#]').click(function (e) {
        e.preventDefault();
    });

    /* slider */
    $('.add-line').append('<span class="line"></span>');
    slider.init();

    $('.rect-down').click(function (e) {
        e.preventDefault();
        $.scrollify.next();
    });

    /* scrollify */
    //if ($('body').hasClass('home-page')) {
    var scrollifyParams = {
        section: ".scroll-section",
        sectionName: "section-name",
        easing: "easeInOutQuint",
        scrollSpeed: 1100,
        offset: -60,
        scrollbars: true,
        before: function () {
        },
        after: function () {
        },
        afterResize: function () {
            if (window.innerWidth <= 991) {
                $.scrollify.destroy();
            }
        }
    };


    if (!isMobile.any || window.innerWidth > 991) {
        $.scrollify(scrollifyParams);
    }


    var screen = window.innerWidth < 992 ? 'small' : 'large';
    $(window).on('resize', function () {
        if (!isMobile.any)
            window.location.reload();
    });
    //}

    $(window).on('scroll', scrollElVis);
    scrollElVis();

    /* main menu */
    $('menu').append('<li class="hover-line"></li>');
    var $hover_line = $('menu li.hover-line');
    var $this = false;
    var $active = $('menu .active').toArray();

    if ($active.length > 0 == false) {
        $hover_line.css({
            'width': '0px'
        });
    }
    else {
        $active = $('menu .active');
        var position = $active.position();

        $hover_line.css({
            'left': (position.left + parseInt($active.css('padding-left'))) + 'px',
            'width': $active.width() + 'px'
        });
    }

    $('menu li:not(.hover-line)').hover(
            function () {
                $this = $(this);
                var position = $this.position();
                $hover_line.css({
                    'left': (position.left + parseInt($this.css('padding-left'))) + 'px',
                    'width': $this.width() + 'px'
                });
            },
            function () {
                if ($active.length > 0 == false) {
                    $hover_line.css({
                        'width': '0px'
                    });
                }
                else {
                    $active = $('.active', $this.parent());
                    var position = $active.position();

                    $hover_line.css({
                        'left': (position.left + parseInt($active.css('padding-left'))) + 'px',
                        'width': $active.width() + 'px'
                    });
                }
            }
    );

    $('.c-hamburger').click(function () {
        $('#mobile-menu').toggleClass('opened');
    });



    /* property page */

    /* property slider */
    $('.property-slider').each(function () {

        var $this = $(this);
        var size = $('> div', this).size();
        var $arrows = $('.prop-slider-nav', $this.parents('.item'));

        if (size <= 1) {
            $arrows.hide();
        }
    });

    $('.property-slider').slick({
        dots: false,
        infinite: true,
        speed: 600,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        swipeToSlide: true,
        cssEase: "cubic-bezier(0.77, 0, 0.175, 1)"
    });



//    $('.pr-large-image').on('DOMMouseScroll mousewheel', function (e) {
//        //$('.property-menu a').click();
//        if (e.originalEvent.wheelDelta / 120 > 0 === false) {
//            e.preventDefault();
//            var body = $("html, body");
//            body.stop().animate({scrollTop: 800}, '1100', 'easeInOutQuint', function () {
//                //alert("Finished animating");
//            });
//        }
//
//    });


    $('.property-container .prop-slider-nav a').click(function (e) {
        e.preventDefault();

        var $item = $(this).parents('.item');
        var $slider = $('.property-slider', $item);

        var arrClass = $(this).hasClass('arrow-left') ? '.slick-prev' : '.slick-next';
        $(arrClass, $slider).click();
    });



    var items = $('.property-right-side .item').toArray();
    var flag = true;
    var links = $('.property-menu a').toArray();
    var index = 0;

    if (items.length > 0) {
        $('.property-right-side .item').hide();
        $(items[0]).show();
    }

    $('.property-preloader').on("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd",
            function () {
                $(this).removeClass('action');
                flag = true;
            });

    $('.item').each(function (i) {
        $(this).attr('id', 'item-' + i);
    });

    $('.property-menu a')
            .each(function (i) {
                $(this).attr('href', '#item-' + i).attr('data-index', i);
            })
            .click(function (e) {
                e.preventDefault();
                var $this = $(this);
                $('.property-preloader').addClass('action');
                $('.property-menu li').removeClass('active');
                $this.parent().addClass('active');

                index = parseInt($this.data('index'));

                setTimeout(function () {
                    $('.item').hide();
                    $($this.attr('href')).show();
                    $('.prop-slider-nav .arrow-right').click();
                }, 1000);
            });

    $('header').on('DOMMouseScroll mousewheel', function () {
        $.scrollify.enable();
    });

    $(document).on('scroll', function () {
        $.scrollify.enable();
    });

    $('.property-content').on('DOMMouseScroll mousewheel', function (e, delta) {
        var delta = delta || -e.originalEvent.detail / 3 || e.originalEvent.wheelDelta / 120;

        if (delta > 0 && flag) {
            if (--index < 0) {
                index = 0;
                $.scrollify.enable();
                return;
            }
            flag = false;
        }
        else if (flag) {
            if (++index >= links.length) {
                index = links.length - 1;
                $.scrollify.enable();
                return;
            }
            flag = false;
        }

        if (!flag) {
            $(links[index]).click();
            e.preventDefault();

            $.scrollify.disable();
        }
    });
});


$(window).load(function () {

});



function elementInViewport(el) {
    var top = el.offset().top;
    var bottom = el.offset().top + el.outerHeight();
    var viewPort = $(window).scrollTop() + $(window).innerHeight();

    return viewPort > top && $(window).scrollTop() < bottom;
}

//function elementInViewport(el) {
//    var top = el.offsetTop;
//    var left = el.offsetLeft;
//    var width = el.offsetWidth;
//    var height = el.offsetHeight;
//
//    while (el.offsetParent) {
//        el = el.offsetParent;
//        top += el.offsetTop;
//        left += el.offsetLeft;
//    }
//
//    return (
//            top >= window.pageYOffset &&
//            left >= window.pageXOffset &&
//            (top + height) <= (window.pageYOffset + window.innerHeight) &&
//            (left + width) <= (window.pageXOffset + window.innerWidth)
//            );
//}


function scrollElVis() {
    $('.fx-section').each(function () {
        if (elementInViewport($(this))) {
            var $this = $(this);

            setTimeout(function () {
                $this.addClass('visible');
            }, 400);
        }
    });
}


(function () {

    "use strict";

    var toggles = document.querySelectorAll(".c-hamburger");

    for (var i = toggles.length - 1; i >= 0; i--) {
        var toggle = toggles[i];
        toggleHandler(toggle);
    }
    ;

    function toggleHandler(toggle) {
        toggle.addEventListener("click", function (e) {
            e.preventDefault();
            (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
        });
    }

})();


/* cookie */
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
            ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

function deleteCookie(name) {
    setCookie(name, "", {
        expires: -1
    })
}