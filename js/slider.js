var slider = {
    scrolling: false,
    interval: 6000,
    intervalID: false,
    id: '#slider',
    this: $('#slider'),
    slidersCont: 0,
    activeSlide: 1,
    blocks: {
        left: $('.sl-block-left'),
        right: $('.sl-block-right'),
        slide: $('.slide'),
        content: $('.sl-content')
    },
    init: function () {
        slider.slidersCont = slider.getSlidersCount();

        if (slider.slidersCont <= 0) {
            console.log('Error! No sliders!');
            return;
        }

        $('.slide', slider.blocks.left).each(function (i) {
            $(this).attr('data-slide', (i + 1).toString());
        });
        $('.slide', slider.blocks.right).each(function (i) {
            $(this).attr('data-slide', (i + 1).toString());
        });

        var slides = $('.slide', slider.blocks.left).get().reverse();
        slider.blocks.left.append(slides);

        slider.initNav();

        slider.initHandlers();

        setTimeout(function () {
            slider.this.addClass('sl-transition');
        }, 500);

        slider.startSlider();

    },
    getSlidersCount: function () {
        var arr = $('.slide', slider.blocks.left).toArray();
        return arr.length;
    },
    startSlider: function () {
        if (slider.intervalID)
            clearInterval(slider.intervalID);

        slider.setActiveSlide();
        slider.initMargins();

        slider.intervalID = setInterval(function () {
            //slider.blocks.right.css('margin-top', '-' + 100 + 'vh');
            slider.activeSlide++;
            slider.initMargins();
            slider.setActiveMenu();
            slider.setActiveSlide();
        }, slider.interval);
    },
    initMargins: function () {
        var left;
        var right;
        var property = 'margin-top';
        var inches = 'vh';

        slider.blocks.left.removeAttr('style');
        slider.blocks.right.removeAttr('style');


        if (slider.activeSlide > slider.slidersCont)
            slider.activeSlide = 1;

        if (window.innerWidth < 992 && deviceOrientation() === 'portrait') {
            slider.blocks.left.css('width', slider.slidersCont * 100 + 'vw');
            slider.blocks.right.css('width', slider.slidersCont * 100 + 'vw');
            property = 'margin-left';
            inches = 'vw';
        }

        left = '-' + ((slider.slidersCont - slider.activeSlide) * 100) + inches;
        slider.blocks.left.css(property, left);

        right = '-' + ((slider.activeSlide - 1) * 100) + inches;
        slider.blocks.right.css(property, right);

        console.log(left);
    },
    initNav: function () {
        slider.this.append('<ul class="sl-nav"></ul>');
        for (var i = 0; i < slider.slidersCont; i++) {
            $('.sl-nav').append('<li><a href="#" data-slide="' + (i + 1) + '"></a></li>');
        }

        slider.setActiveMenu();
    },
    setActiveMenu: function () {
        $('.sl-nav a').removeClass('active');
        $('.sl-nav a[data-slide="' + slider.activeSlide + '"]').addClass('active');
    },
    setActiveSlide: function () {
        $('.slide').removeClass('active');
        setTimeout(function () {
            $('.slide[data-slide="' + slider.activeSlide + '"]').addClass('active');
        }, 800);
    },
    initHandlers: function () {
        $('.sl-nav a').click(function (e) {
            e.preventDefault();
            slider.activeSlide = parseInt($(this).data('slide'));
            slider.startSlider();
            slider.setActiveMenu();
        });

        $(window).on('resize', function () {
            slider.startSlider();
        });
    }
};


function deviceOrientation() {
    return window.innerHeight < window.innerWidth ? 'landscape' : 'portrait';
};